'''-------------------------------------------------------------------------------
 Source Name: DEA_Image_extractByDate_updated.py
 Version:     Digital Earth Australia
 Author:      Samantha Dawson/Nick Streeton
 Updated by:
 Description: Extract images (both landsat and sentinel) from DEA for a given polygon
                 Save outputs to AWS s3 Bucket
 History:     Initial coding - 20/08/2020, version 1.0. Based on script DEA_Image_extraction_by_date
                                        with updated DEA commands
 Updated:     Version 1.0, 2/02/2023, code up and running but need permission to extract Sentinel 2 Collection 3
 Notes:        Before running this code on the NCI: bash# module load dea
-------------------------------------------------------------------------------'''

import sys, os
import datacube
import geopandas as gpd
import rasterio
import xarray as xr
import boto3
from datacube.utils import geometry
from datacube.utils.cog import write_cog
import sys
import datacube
import geopandas as gpd
import rasterio
import xarray as xr
from datacube.utils import geometry
from datacube.utils.cog import write_cog

sys.path.insert(1, '../Tools')
##################
#Nick - not sure if I should soft code this too?
sys.path.append('/home/008/sd4815/dea-notebooks_NCIversion/Tools')
##################
from dea_tools.datahandling import load_ard
from dea_tools.spatial import xr_rasterize
from dea_tools.plotting import rgb

#Load the Polygon drill app from DEA (https://docs.dea.ga.gov.au/notebooks/Frequently_used_code/Polygon_drill.html)
#and the load ard app (https://docs.dea.ga.gov.au/notebooks/Frequently_used_code/Using_load_ard.html)
dc = datacube.Datacube(app='Polygon_drill')
dc = datacube.Datacube(app='Using_load_ard')
dc = datacube.Datacube(app='Filtering_datasets')

#################### INPUTS ################################

#1. set the path directory
	#Note: this folder must have the following folders:
	#Boundaries - with the boundary shapefile
	#Scripts - with this python script
	#Outputs - where the outputs will be temporarily stored
    #Also need to set the aws bucket it is going into as not allowed to have the same named bucket

#filepath = '/home/008/sd4815/dea-notebooks_NCIversion/Sam_DEA_updates/'
filepath = '/home/550/ns6732/dea-notebooks-stable/'

aws_bucket = 'data-cube-outputs'

#2. set the boundary polygon 'Boundaries/.shp 
	#Must shapefile must contain a field titled "NAME" with values
	#Must be projected in WGS 1984

polygon_to_drill = os.path.join(filepath, ('Boundaries/Yellow_Clay.shp'))

#3. set the date range within a list []
	# ranges should be enclosed in brackets (tuples), unique dates comma seperated: [('2016-08', '2016-09'),'2016-09-15']
	# dates can be as a whole year: ['2016'], a month: ['2016-09'] or a specific date: ['2016-09-15']
	# to get a range of dates use: [('2016-09','2016-11')] e.g. Sept through to November
	# to select multiple date ranges use: month_filter = 'True' and then use month_select to identify months (e.g. (2,8) selects Feb and Aug)
	# to select seasonal use: same ase above, but use the seasonal months
	# to select seasonal over several years use: use the month_filter above, but select the year range of interest (e.g. ('2016','2018') with a
    # selection of summer months will pull out summer images from 2016-2018
	
list_of_dates = [('2016-08', '2016-09'),'2016-09-15']

#3A. set True if you want to use a particular set of months.
	#Note summer = (12,1,2), autumn = (3,4,5), winter = (6,7,8), spring = (9,10,11)
month_filter = 'False'
month_select = (6,7,8) 
          
#4. set cloud filtering percentage
min_gooddata = 0.5 #set if you want to remove images with high cloud cover (i.e. greater than 50% = 0.5). NOTE: all images have cloud/pixel quality masks applied already.

#5. select sensors to use, True if required
sensorLandsat = 'True'
sensorSentinel = 'True'

################No need to change anything after here#####################

polygon_to_drill = gpd.read_file(polygon_to_drill)

print("Now processing the following date ranges " + str(list_of_dates))

#run the geom so it goes through every polygon in the shapefile and keeps the name
def getCatchmentsCount(polygon_to_drill):
    catch_numb = len(polygon_to_drill.NAME)
    return catch_numb

def exportToAWS(fileName, bucketName, outputName):
    s3_client = boto3.client('s3')
    s3_client.upload_file(fileName,bucketName,outputName)
    os.remove(fileName)

#define seasonsal/month filters:
def filter_month(dataset):
    return dataset.time.begin.month in (month_select)

#Create a query for the datacube using this polygon.
#Note: the following parameters need to be specified:
#'geopolygon': Here we input the geometry we want to use for the drill that we prepared in the cell above
#'time': Feed in the time_to_drill parameter we set earlier
#'output_crs': We need to specify the coordinate reference scheme of the output. We'll use Albers Equal Area projection for Australia
#'resolution': You can choose the resolution of the output dataset.
#               NOTE: if you use 30 for sentinel data (i.e. call sentinel and landsat in the same query
#                   it will convert the 10m cells to 30m cells
#'measurements': Here is where you specify which bands you want to extract.
#                   As a default we are selecting all bands
#                   See: https://docs.dea.ga.gov.au/notebooks/Beginners_guide/02_DEA.html#DEA-band-naming-conventions
#In addition, we can add a cloud cover query to remove images with more than a certain amount of cloud cover:
#'cloud_cover' : Use this to select an acceptable range of cloud cover - e.g. 0-50% = (0, 50)

def landsat_func(geom, time_to_drill):
    if month_filter == 'True':
        query_landsat = {'geopolygon': geom,
             'time': time_to_drill,
             'output_crs': 'EPSG:3577',
             'resolution': (-30, 30),
             'dataset_predicate': filter_month,
             'measurements': [#'nbart_coastal_aerosol',
                              'nbart_blue',
                              'nbart_green',
                              'nbart_red',
                              'nbart_nir',
                              'nbart_swir_1',
                              'nbart_swir_2',
                              'nbart_panchromatic',
                              'oa_fmask']
                }
    else:
        query_landsat = {'geopolygon': geom,
             'time': time_to_drill,
             'output_crs': 'EPSG:3577',
             'resolution': (-30, 30),
             'measurements': [#'nbart_coastal_aerosol',
                              'nbart_blue',
                              'nbart_green',
                              'nbart_red',
                              'nbart_nir',
                              'nbart_swir_1',
                              'nbart_swir_2',
                              'nbart_panchromatic',
                              'oa_fmask']
                }
    return query_landsat

def sentinel_func(geom, time_to_drill):
    if month_filter == 'True':
        query_sentinel = {'geopolygon': geom,
             'time': time_to_drill,
             'output_crs': 'EPSG:3577',
             'resolution': (-10, 10),
             'dataset_predicate': filter_month,
             'measurements': ['nbart_coastal_aerosol',
                              'nbart_blue',
                              'nbart_green',
                              'nbart_red',
                              'nbart_red_edge_1',
                              'nbart_red_edge_2',
                              'nbart_red_edge_3',
                              'nbart_nir_1',
                              'nbart_nir_2',
                              'nbart_swir_2',
                              'nbart_swir_3']
                }
    else:
        query_sentinel = {'geopolygon': geom,
             'time': time_to_drill,
             'output_crs': 'EPSG:3577',
             'resolution': (-10, 10),
             'measurements': ['nbart_coastal_aerosol',
                              'nbart_blue',
                              'nbart_green',
                              'nbart_red',
                              'nbart_red_edge_1',
                              'nbart_red_edge_2',
                              'nbart_red_edge_3',
                              'nbart_nir_1',
                              'nbart_nir_2',
                              'nbart_swir_2',
                              'nbart_swir_3']
                }
    return query_sentinel

#loop through the unique polygon areas to be queried
num_catchments = getCatchmentsCount(polygon_to_drill)

#extract the data and output to AWS
for i in range(num_catchments):
    geom_crs = polygon_to_drill.crs
    geom = geometry.Geometry(geom=polygon_to_drill.iloc[i].geometry,
                             crs=polygon_to_drill.crs)
    catchment_name = polygon_to_drill.NAME[i].replace(" ", "_")

    print("Now processing the following study area... " + catchment_name)

    # loop through each date range
    for time_to_drill in list_of_dates:
        # to go through each sensor and add a variable that is the sensor name:
        if sensorLandsat == 'True':
            for sensor in ['ga_ls5t_ard_3', 'ga_ls7e_ard_3', 'ga_ls8c_ard_3']:
                try:

                    # Load data
                    query_landsat = landsat_func(geom, time_to_drill)
                    dataLS = load_ard(dc=dc, products=[sensor], group_by='solar_day',min_gooddata=min_gooddata, **query_landsat)

                    # Mask data
                    print('Masking for the following sensor: ' + sensor)
                    mask = xr_rasterize(polygon_to_drill.iloc[[i]], dataLS)
                    dataLS_masked = dataLS.where(mask)
                    # Set sensor attribute
                    dataLS_masked.attrs['sensor'] = sensor
                    # Set catchment name
                    dataLS_masked.attrs['catchment'] = catchment_name

                    #iterate through each date in the xarray and export to AWS
                    for timestep in range(len(dataLS_masked.time)):

                    # We will use the date of the satellite image to name the GeoTIFF
                        date = dataLS_masked.isel(time=timestep).time.dt.strftime('%Y-%m-%d').data
                        print(f'Writing {date}')

                        # Convert current time step into a `xarray.DataArray`
                        singletimestamp_da = dataLS_masked.isel(time=timestep).to_array()

                        #Extra info in file name
                        outputFilePath = os.path.join(filepath, ('Outputs/'))# temporary file on NCI to store images
                        fname = f'{catchment_name}_{sensor}_{date}.tif'
                        dsname = outputFilePath+fname

                        # Write GeoTIFF
                        write_cog(geo_im=singletimestamp_da,fname=dsname, overwrite=True)

                        exportToAWS(dsname, 'data-cube-outputs', fname)
                        print('Wrote {} to GeoTiff and exported to AWS'.format(fname))

                except ValueError:
                    print('no data available for query')

        # to go through each sentinel sensor and add a variable that is the sensor name:
        if sensorSentinel == 'True':
            for sensor_s in ['ga_s2am_ard_3', 'ga_s2bm_ard_3']:
                try:
                    # Load data
                    query_sentinel = sentinel_func(geom, time_to_drill)
                    dataSent = load_ard(dc=dc, products=[sensor_s], group_by='solar_day', min_gooddata=min_gooddata, **query_sentinel)

                    # Mask data
                    print('Masking for the following sensor: ' + sensor_s)
                    mask = xr_rasterize(polygon_to_drill.iloc[[i]], dataSent)
                    dataSent_masked = dataSent.where(mask)
                    # Set sensor attribute
                    dataSent_masked.attrs['sensor_s'] = sensor_s
                    # Set catchment name
                    dataSent_masked.attrs['catchment'] = catchment_name

                    #iterate through each date in the xarray and export to AWS
                    for timestep in range(len(dataSent_masked.time)):

                    # We will use the date of the satellite image to name the GeoTIFF
                        date = dataSent_masked.isel(time=timestep).time.dt.strftime('%Y-%m-%d').data
                        print(f'Writing {date}')

                        # Convert current time step into a `xarray.DataArray`
                        singletimestamp_da = dataSent_masked.isel(time=timestep).to_array()

                        #Extra info in file name
                        outputFilePath = os.path.join(filepath, ('Outputs/')) # temporary file on NCI to store images
                        fname = f'{catchment_name}_{sensor_s}_{date}.tif'
                        dsname = outputFilePath+fname

                        # Write GeoTIFF
                        write_cog(geo_im=singletimestamp_da,fname=dsname, overwrite=True)

                        exportToAWS(dsname, aws_bucket, fname)
                        print('Wrote {} to GeoTiff and exported to AWS'.format(fname))

                except ValueError:
                    print('no data available for query')
 

    print("Finished finding and masking data")

