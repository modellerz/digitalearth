import os, boto3

# AWS session and bucket
session = boto3.Session(profile_name='default')
s3 = boto3.resource('s3')
bucket = s3.Bucket('data-cube-outputs')

for item in bucket.objects.all():
    filename =  os.path.join('F://', item.key)
    print filename
    bucket.download_file(item.key, filename)