'''-------------------------------------------------------------------------------
 Source Name: GMW_NDVI_Demo.py
 Version:     Digital Earth Australia
 Author:      Jacobs
 Updated by:  Nick Streeton <nicholas.streeton@jacobs.com>
 Description: Create a series of NDVI rasters (monthly)(statistical max)
                 Iterate through multiple catchments (in shapefile - set Line 44)
                 Save outputs to AWS s3 Bucker
 History:     Initial coding - 05/12/2018, version 1.0
 Updated:     Version 1.0, XX/XX/2018, explanation
 Notes:        Before running this code on the NCI: bash# module load dea
-------------------------------------------------------------------------------'''

# %matplotlib inline
import sys, os
import warnings
import datacube
import matplotlib.pyplot as plt
from datacube.storage import masking
from datacube.helpers import write_geotiff
from shapely.geometry import shape

import boto3
import fiona
import datetime

# Import dea-notebook functions. This assumes you are running this notebook from inside the `dea-notebooks`
# directory. If not, replace the relative `../Scripts` link with a path to the `dea-notebooks/Scripts` directory
sys.path.append('/home/550/ns6732/dea-notebooks/10_Scripts')
import DEAPlotting
import DEADataHandling

from datacube_stats.statistics import GeoMedian
# For nicer notebook plotting, hide warnings (comment out for real analysis)
warnings.filterwarnings('ignore')

def exportToAWS(fileName, bucketName, outputName):
	s3_client = boto3.client('s3')
	s3_client.upload_file(fileName,bucketName,outputName)

def getCatchments():
	catchment_boundaries = os.path.expanduser('~/dea-notebooks/GMID/Boundaries/GMW_Boundary_WGS84.shp')
	sub_boundaries = fiona.open(catchment_boundaries)
	return sub_boundaries

def geom_query(geom, geom_crs):
    """
    Create datacube query snippet for geometry
    """
    return {
        'x': (geom.bounds[0], geom.bounds[2]),
        'y': (geom.bounds[1], geom.bounds[3]),
        'crs': geom_crs
    	}

def last_day_of_month(any_day):
    next_month = any_day.replace(day=28) + datetime.timedelta(days=4)
    return next_month - datetime.timedelta(days=next_month.day)

def developQuery(month):
	# for the year 2005
	first_day = datetime.date(2005,month,1)
	last_day= last_day_of_month(datetime.date(2005, month, 1))
	#queryString = (first_day) + ', ' + str(last_day)
	return first_day, last_day

def getMonthName(month):
	month = datetime.date(1900, month, 1).strftime('%b')
	return month

def calculateNDVI():
	# Connect to a datacube
	dc = datacube.Datacube(app='GMID NDVI Demo')

	# Iterate each sub-catchment
	sub_boundaries = getCatchments()
	for i in range(len(sub_boundaries)):

		geom_crs = sub_boundaries.crs_wkt

		geom = shape(sub_boundaries[i]['geometry'])
		print(geom)
		
		catchment_name = sub_boundaries[i]['properties']['DESCRIPTIO']
		catchment_name_output = catchment_name.replace(" ", "_")
		print("Now processing..... " + catchment_name)
		
		# set up storage lists


		# Iterate through each month (Jan - Dec)
		for month in range(1,13):
			stats_file=[]
			file_name=[]
			# Generate output name
			NDVI_mth_output = 'GMID_' + catchment_name_output + '_2005_' + getMonthName(month) + '_NDVI_max'
			print(NDVI_mth_output)
			file_name.append(NDVI_mth_output)
			# Develop DEA Query String
			query = developQuery(month)
			query = {
				'time':(developQuery(month)[0], developQuery(month)[1]),
    	     	}
			query.update(geom_query(geom, geom_crs))
			print(query)
			sensor = 'ls5'
			ds, crs, affine = DEADataHandling.load_nbarx(dc=dc, query=query, sensor=sensor, product='nbar', 			bands_of_interest=['red', 'nir'], filter_pq=True)
			if ds is None:
				print('no data loaded from DEA - lack of data')
				continue
			else:
				print('loaded data from DEA')
				print(ds.time)

	#fc, crs, affine = DEADataHandling.load_clearlandsat(dc=dc, query=query, product='fc', masked_prop=0.80)
	
			# Calculate Landsat NDVI
			ds_ndvi = (ds.nir - ds.red) / (ds.nir + ds.red)
	
			# Calculate the maximum NDVI for each month
			NDVI_mth_output=ds_ndvi.max(dim='time')
			stats_file.append(NDVI_mth_output)
			
			# export results for each timestep before clearing list
			print("Exporting outputs to AWS")
			GenerateOuputTIF(file_name, stats_file, ds, crs, affine)

def GenerateOuputTIF(file_names, stats_files, ds, crs, affine):
	outputFilePath = '/home/550/ns6732/dea-notebooks/GMID/outputs/'
	for statistic_output in range(len(stats_files)):
		dataset = stats_files[statistic_output].to_dataset(name=file_names[statistic_output])
		dataset.attrs['affine']=ds.affine
		dataset.attrs['crs']=crs
		fileName = outputFilePath+file_names[statistic_output]+'.tif'
		outputName = file_names[statistic_output]+'.tif'

		write_geotiff(fileName, dataset)

		print('wrote', file_names[statistic_output], ' to ' + outputFilePath)
		exportToAWS(fileName, 'data-cube-outputs', outputName)
		print('copied', outputName, 'to AWS bucket')
		os.remove(fileName)


if __name__ == "__main__":
	print("Loading DEA and calculating NDVI....")
	calculateNDVI()


