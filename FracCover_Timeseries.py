'''-------------------------------------------------------------------------------
 Source Name: Fraccover_Timeseries.py
 Version:     Digital Earth Australia
 Author:      Samantha Dawson
 Updated by:
 Description: -Extract fractional cover (landsat) from DEA for a given set of investigation and reference sites
              -for investigation sites, get the combined fractional cover euclidean distance between investigation
                sites on a site and pixel level for each time period
              -outputs: plots showing the euclidean distance at a pixel level for each site and time period
                        table in image format of the mean euclidean distance, std and difference between times
                            at a site level
                        .csv of the mean, std and difference values shown in above table
                        Nick: do you want the data that goes into the plots as well? It is quite a bit, but might be handy...
 History:     Initial coding - made on DEA Sandbox based on the analysis done in Dawson et al. 2016
                    (https://www.mdpi.com/2072-4292/8/7/542)
 Updated:     Version 1.0, 08/06/2023, code up and running but need permission to extract Sentinel 2 Collection 3
 Notes:        Before running this code on the NCI: bash# module load dea
-------------------------------------------------------------------------------'''

#Load and open the necessary tools
import sys
import datacube
import geopandas as gpd
import rasterio
import xarray as xr
import numpy as np
import pandas as pd
import os
import matplotlib.pyplot as plt
from datacube.utils import geometry, masking
from datacube.utils.cog import write_cog

sys.path.insert(1, '../Tools')
from dea_tools.datahandling import load_ard, wofs_fuser
from dea_tools.spatial import xr_rasterize
from dea_tools.plotting import rgb, plot_wo

#Connnect to datacube to access DEA datasets
dc = datacube.Datacube(app='Polygon_drill')
dc = datacube.Datacube(app='Using_load_ard')
dc = datacube.Datacube(app='Filtering_datasets')
dc = datacube.Datacube(app='DEA_Fractional_Cover')

#################### INPUTS ################################

#1. set the path directory
	#Note: this folder must have the following folders:
	#Boundaries - with the shapefile of the boundary of reference and investigation sites
	#Scripts - with this python script
	#Outputs - where the outputs will be temporarily stored
    #Also need to set the aws bucket it is going into as not allowed to have the same named bucket

filepath = '/home/008/sd4815/dea-notebooks_NCIversion/Sam_DEA_FracCover/'
#filepath = '/home/550/ns6732/dea-notebooks-stable/'

aws_bucket = 'data-cube-outputs'

#2. set the polygon shapefile  'Boundaries/.shp
	#Must shapefile must contain a field titled "NAME" with values
	#Must be projected in WGS 1984

polygon_to_drill = 'BB_polys_reproj.shp' #path_to_shapefile
polygon_to_drill = gpd.read_file(polygon_to_drill)
print(polygon_to_drill.head(3))

#3. set the date range within a list []
	# ranges should be enclosed in brackets (tuples), unique dates comma seperated: [('2016-08', '2016-09'),'2016-09-15']
	# dates can be as a whole year: ['2016'], a month: ['2016-09'] or a specific date: ['2016-09-15']
	# to get a range of dates use: [('2016-09','2016-11')] e.g. Sept through to November
	# to select multiple date ranges use: month_filter = 'True' and then use month_select to identify months (e.g. (2,8) selects Feb and Aug)
	# to select seasonal use: same ase above, but use the seasonal months
	# to select seasonal over several years use: use the month_filter above, but select the year range of interest (e.g. ('2016','2018') with a
    # selection of summer months will pull out summer images from 2016-2018
time_to_drill1 = ('2000-01-01', '2002-12-31')
time_to_drill2 = ('2012-01-01', '2013-12-31')

#3A. set True if you want to use a particular set of months.
	#Note summer = (12,1,2), autumn = (3,4,5), winter = (6,7,8), spring = (9,10,11)
month_filter = 'True'
month_select = (3,4,5,6) #set if you want to use a particular set of months.
                       #Note summer = (12,1,2), autumn = (3,4,5), winter = (6,7,8), spring = (9,10,11)

#4. set percentage of no_data
#Use this to filter to only have observations with, e.g. less than 30% nodata - which is the usual amount
min_nodata = 0.3

################No need to change anything after here#####################

#Notes on fractional cover extraction
'''
### Query the datacube using this polygon.
Note: the following parameters need to be specified:

- `'geopolygon'`: Here we input the geometry we want to use for the drill that we prepared in the cell above
- `'time'`: Feed in the `time_to_drill` parameter we set earlier
- `'output_crs'`: We need to specify the coordinate reference scheme of the output.
We'll use Albers Equal Area projection for Australia
- `'resolution'`: You can choose the resolution of the output dataset. In this case the Fractional Cover is based on Landsat - 30m x 30m

Website for Fractional cover info:
https://www.dea.ga.gov.au/products/dea-fractional-cover
'''

def exportToAWS(fileName, bucketName, outputName):
    s3_client = boto3.client('s3')
    s3_client.upload_file(fileName,bucketName,outputName)
    os.remove(fileName)

# get the number of catchments
def getCatchmentsCount(polygon_to_drill):
    catch_numb = len(polygon_to_drill.NAME)
    return catch_numb

# define seasonsal/month filters:
def filter_month(dataset):
    return dataset.time.begin.month in (month_select)

# define the measurements to be extracted:
def FC_func(geom, time_to_drill):
    if month_filter == 'True':
        query_FC = {'geopolygon': geom,
                    'time': time_to_drill,
                    'output_crs': 'EPSG:32754',
                    'resolution': (-30, 30),
                    'dataset_predicate': filter_month,
                    'measurements': ['bs',
                                     'pv',
                                     'npv',
                                     'ue']
                    }
    else:
        query_FC = {'geopolygon': geom,
                    'time': time_to_drill,
                    'output_crs': 'EPSG:32754',
                    'resolution': (-30, 30),
                    'measurements': ['bs',
                                     'pv',
                                     'npv',
                                     'ue']
                    }
    return query_FC

# create a time 1 and time 2 array
FC_t1_data4ana = []
FC_t2_data4ana = []

# For each time array do the following:
# extract the fractional cover vegetation from dea,
# mask out pixels with water or clouds,
# select scenes with less than 30% of pixels with no data (or whatever min_nodata is set to)
# mask out data outside the polygon but within the extent
num_catchments = getCatchmentsCount(polygon_to_drill)
for i in range(num_catchments):
    geom_crs = polygon_to_drill.crs
    geom = geometry.Geometry(geom=polygon_to_drill.iloc[i].geometry,
                             crs=polygon_to_drill.crs)
    catchment_name = polygon_to_drill.NAME[i].replace(" ", "_")
    site_type = polygon_to_drill.Site_Type[i].replace(" ", "_")
    # FC_datappend = []

    print("Now extracting fractional covers for time 1..... " + catchment_name)

    # to go through each catchmment and add a variable that is the catchment name:
    try:
        # Load data
        query_FC = FC_func(geom, time_to_drill1)
        dataFC_t1 = dc.load(product='ga_ls_fc_3',
                            group_by='solar_day',
                            **query_FC)
        # Replace all nodata values with `NaN`
        dataFC_t1 = masking.mask_invalid_data(dataFC_t1)
        # mask out clouds and open water (wofs)
        # first load wofs data for area of interest
        wo = dc.load(product='ga_ls_wo_3',
                     group_by='solar_day',
                     fuse_func=wofs_fuser,
                     like=dataFC_t1)
        # then make a set of masks based on non-cloudy/non-water pixels
        wo_mask = masking.make_mask(wo.water, dry=True)
        # apply these masks to data
        dataFC_t1_clean = dataFC_t1.where(wo_mask)
        # Calculate the percent of nodata pixels in each observation
        percent_nodata = dataFC_t1_clean.pv.isnull().mean(dim=['x', 'y'])
        # Use this to filter to only have observations with less than 30% nodata
        dataFC_t1_clean = dataFC_t1_clean.sel(time=percent_nodata < min_nodata)
        # Mask data to shapefile
        print('Masking for..... ' + catchment_name)
        mask = xr_rasterize(polygon_to_drill.iloc[[i]], dataFC_t1)
        dataFC_t1_masked = dataFC_t1_clean.where(mask)
        # Set catchment name
        dataFC_t1_masked.attrs['catchment'] = catchment_name
        # Set site type
        dataFC_t1_masked.attrs['site'] = site_type
        # Add to xarray
        FC_t1_data4ana += [dataFC_t1_masked]
    except ValueError:
        print('no data available for query')

    print("Now extracting fractional covers for time 2..... " + catchment_name)

    # to go through each catchmment and add a variable that is the catchment name:
    try:
        # Load data
        query_FC = FC_func(geom, time_to_drill2)
        dataFC_t2 = dc.load(product='ga_ls_fc_3',
                            group_by='solar_day',
                            **query_FC)
        # Replace all nodata values with `NaN`
        dataFC_t2 = masking.mask_invalid_data(dataFC_t2)
        # mask out clouds and open water (wofs)
        # first load wofs data for area of interest
        wo = dc.load(product='ga_ls_wo_3',
                     group_by='solar_day',
                     fuse_func=wofs_fuser,
                     like=dataFC_t2)
        # then make a set of masks based on non-cloudy/non-water pixels
        wo_mask = masking.make_mask(wo.water, dry=True)
        # apply these masks to data
        dataFC_t2_clean = dataFC_t2.where(wo_mask)
        # Calculate the percent of nodata pixels in each observation
        percent_nodata = dataFC_t2_clean.pv.isnull().mean(dim=['x', 'y'])
        # Use this to filter to only have observations with less than 30% nodata
        dataFC_t2_clean = dataFC_t2_clean.sel(time=percent_nodata < min_nodata)
        # Mask data to shapefile
        print('Masking for..... ' + catchment_name)
        mask = xr_rasterize(polygon_to_drill.iloc[[i]], dataFC_t2)
        dataFC_t2_masked = dataFC_t2_clean.where(mask)
        # Set catchment name
        dataFC_t2_masked.attrs['catchment'] = catchment_name
        # Set site type
        dataFC_t2_masked.attrs['site'] = site_type
        # Add to xarray
        FC_t2_data4ana += [dataFC_t2_masked]
    except ValueError:
        print('no data available for query')

print("Finished finding and masking data")

# then get the mean pixel value for all fractional covers in each polygon / time period
# note: the number of locations (n_loc) should be the same for each of the time period, so can use just one
n_loc = len(FC_t1_data4ana)

# copy the datasets to edit
mean_t1 = FC_t1_data4ana.copy()
std_t1 = FC_t1_data4ana.copy()
mean_t2 = FC_t2_data4ana.copy()
std_t2 = FC_t2_data4ana.copy()

# note: we use skipna=True to account for individual pixels missing from scenes due to, for example, cloud masking
for i in range(n_loc):
    mean_t1[i] = mean_t1[i][['pv', 'npv', 'bs']].mean(dim=['time'], keep_attrs=True, skipna=True)
    std_t1[i] = std_t1[i][['pv', 'npv', 'bs']].mean(dim=['time'], keep_attrs=True, skipna=True)
    mean_t2[i] = mean_t2[i][['pv', 'npv', 'bs']].mean(dim=['time'], keep_attrs=True, skipna=True)
    std_t2[i] = std_t2[i][['pv', 'npv', 'bs']].mean(dim=['time'], keep_attrs=True, skipna=True)
print('pixel means for fractional covers calculated')

####then calculate the Euclidean distances for each of the pixel values####

# fist separate out the healthy and invesitgation sites for each of the above

n_sites = len(mean_t1)

print('Total number of sites: ', n_sites)
print('Check there are the same number of sites in each time period:')

# separate the means into investigation and reference sites
ref_sites_mean_t1 = []
invest_sites_mean_t1 = []
ref_sites_mean_t2 = []
invest_sites_mean_t2 = []
idx = np.zeros(n_sites, 'bool')

for i in range(n_sites):
    # ref_sites.append(mean_t1[i].attrs['site'])
    if mean_t1[i].attrs['site'] == 'Healthy_reference_site':
        idx[i] = True
        ref_sites_mean_t1.append(mean_t1[i])
    if mean_t1[i].attrs['site'] == 'Investigation_site':
        idx[i] = True
        invest_sites_mean_t1.append(mean_t1[i])
    if mean_t2[i].attrs['site'] == 'Healthy_reference_site':
        idx[i] = True
        ref_sites_mean_t2.append(mean_t2[i])
    if mean_t2[i].attrs['site'] == 'Investigation_site':
        idx[i] = True
        invest_sites_mean_t2.append(mean_t2[i])

print('Number of healthy reference sites time 1: ', len(ref_sites_mean_t1))
print('Number of investigation sites time 1: ', len(invest_sites_mean_t1))
print('Number of healthy reference sites time 2: ', len(ref_sites_mean_t2))
print('Number of investigation sites time 2: ', len(invest_sites_mean_t2))

# then get the mean pixel value for all fractional covers for each time period for all of the reference sites combined
# so separate out the healthy reference sites for the original pixel value
n_ref_data = len(FC_t1_data4ana)

ref_data_t1 = []
invest_data_t1 = []
ref_data_t2 = []
invest_data_t2 = []

idx = np.zeros(n_sites, 'bool')

for i in range(n_sites):
    # ref_sites.append(mean_t1[i].attrs['site'])
    if FC_t1_data4ana[i].attrs['site'] == 'Healthy_reference_site':
        idx[i] = True
        ref_data_t1.append(FC_t1_data4ana[i])
    if FC_t1_data4ana[i].attrs['site'] == 'Investigation_site':
        idx[i] = True
        invest_data_t1.append(FC_t1_data4ana[i])
    if FC_t2_data4ana[i].attrs['site'] == 'Healthy_reference_site':
        idx[i] = True
        ref_data_t2.append(FC_t2_data4ana[i])
    if FC_t2_data4ana[i].attrs['site'] == 'Investigation_site':
        idx[i] = True
        invest_data_t2.append(FC_t2_data4ana[i])

print('Check there are the same number of reference sites as above:')
print('Number of healthy reference sites time 1: ', len(ref_data_t1))
print('Number of healthy reference sites time 2: ', len(ref_data_t2))

'''
then calculate the mean values for each fractional cover for each time period for the reference sites
i.e. we need one value for each fractional cover to calculate the Euclidean distance to.
This is an unusual operation as we are pulling out the data from its attached spatial metadata (i.e. location)
So below is a slightly hacky way of doing it, but couldn't figure out another
'''

n_ref = len(ref_data_t1)

flatdims = np.zeros(n_ref, 'i4')
for i in range(n_ref):
    nt, ny, nx = ref_data_t1[i]['bs'].shape
    flatdims[i] = nt * ny * nx
dim_sum = np.hstack([0, np.cumsum(flatdims)])
ntot = dim_sum[-1]

pv_flat = np.zeros(ntot)
npv_flat = np.zeros(ntot)
bs_flat = np.zeros(ntot)
ue_flat = np.zeros(ntot)

for i in range(n_ref):
    pv_flat[dim_sum[i]:dim_sum[i + 1]] = ref_data_t1[i]['pv'].to_numpy().ravel()
    npv_flat[dim_sum[i]:dim_sum[i + 1]] = ref_data_t1[i]['npv'].to_numpy().ravel()
    bs_flat[dim_sum[i]:dim_sum[i + 1]] = ref_data_t1[i]['bs'].to_numpy().ravel()
    ue_flat[dim_sum[i]:dim_sum[i + 1]] = ref_data_t1[i]['ue'].to_numpy().ravel()

ref_pv_mean = np.nanmean(pv_flat)
ref_npv_mean = np.nanmean(npv_flat)
ref_bs_mean = np.nanmean(bs_flat)
ref_ue_mean = np.nanmean(ue_flat)

ref_pv_std = np.std(pv_flat)
ref_npv_std = np.std(npv_flat)
ref_bs_std = np.std(bs_flat)
ref_ue_std = np.std(ue_flat)

print('PV mean =', ref_pv_mean, 'NPV mean =', ref_npv_mean, 'BS mean =', ref_bs_mean, 'UE mean =', ref_ue_mean)

'''
Now calculate the euclidean distance for each of the pixels in each of the investigation sites, plot and save out
Notes from Adrian on the Euclidean calculation: 
Calculates a 3 band image of the euclidian distance from the desired time series, one pixel at a time.
The final images are the euclidian distance between each pixel and the desired vegetation time series, 
summed across the three cover fractions, and normalised by dividing by the length of the time series (number of images).

Outputs: the euclidean distance means for each pixel at each site in each time step and their standard devieations
Saves out the plots of each of the sites
'''
# now put the above into a loop to go through all the sites for each timestep
n_ed = len(invest_data_t1)

ed_t1_mean = []
ed_t1_std = []
ed_t2_mean = []
ed_t2_std = []

ed_t1_site_name = []
ed_t1_site_mean = []
ed_t1_site_std = []
ed_t2_site_name = []
ed_t2_site_mean = []
ed_t2_site_std = []

for i in range(n_ed):
    # get the euclidean distance vales across the three fractional covers
    n_images = len(invest_data_t1[i]['time'])
    bare_ed = np.power(np.abs(invest_data_t1[i][['bs']] - ref_bs_mean), 2)
    green_ed = np.power(np.abs(invest_data_t1[i][['pv']] - ref_pv_mean), 2)
    dead_ed = np.power(np.abs(invest_data_t1[i][['npv']] - ref_npv_mean), 2)
    ed_vals = (np.power(bare_ed['bs'], 0.5) + np.power(green_ed['pv'], 0.5) + np.power(dead_ed['npv'], 0.5)) / float(
        n_images)
    ed_vals.attrs = bare_ed.attrs

    # then get the mean and std across time at the pixel level:
    ed_mean = ed_vals.mean(dim=['time'], keep_attrs=True, skipna=True)
    ed_t1_mean.append(ed_mean)
    ed_std = ed_vals.std(dim=['time'], keep_attrs=True, skipna=True)
    ed_t1_std.append(ed_std)
    print('ED calculated for ' + ed_mean.attrs['catchment'])

    # then plot up the mean ed values and save to a plot folder
    outputFilePath = os.path.join(filepath, ('Outputs/'))  # temporary file on NCI to store images
    fname = ed_mean.attrs['catchment'] + '_EDmean_t1.pdf'
    dsname = os.path.join(outputFilePath + fname)

    ed_mean.plot()
    plt.title(ed_mean.attrs['catchment'])  # give the plot title the catchment/site name
    plt.axis('equal')  # to make the pixels square
    plt.savefig(dsname)
    print('plot saved for ' + ed_mean.attrs['catchment'])
    plt.close()

    xportToAWS(dsname, aws_bucket, fname)
    print('Wrote {} to plot and exported to AWS'.format(fname))

    # then get the mean and std across time at the site level:
    ed_site_name = ed_mean.attrs['catchment']
    ed_t1_site_name.append(ed_site_name)
    ed_site_mean = ed_vals.mean(keep_attrs=True, skipna=True)
    ed_t1_site_mean.append(ed_site_mean.values)
    ed_site_std = ed_vals.std(keep_attrs=True, skipna=True)
    ed_t1_site_std.append(ed_site_std.values)

    # repeat for time 2
    # get the euclidean distance vales across the three fractional covers
    n_images = len(invest_data_t2[i]['time'])
    bare_ed = np.power(np.abs(invest_data_t2[i][['bs']] - ref_bs_mean), 2)
    green_ed = np.power(np.abs(invest_data_t2[i][['pv']] - ref_pv_mean), 2)
    dead_ed = np.power(np.abs(invest_data_t2[i][['npv']] - ref_npv_mean), 2)
    ed_vals = (np.power(bare_ed['bs'], 0.5) + np.power(green_ed['pv'], 0.5) + np.power(dead_ed['npv'], 0.5)) / float(
        n_images)
    ed_vals.attrs = bare_ed.attrs

    # then get the mean and std across time:
    ed_mean = ed_vals.mean(dim=['time'], keep_attrs=True, skipna=True)
    ed_t2_mean.append(ed_mean)
    ed_std = ed_vals.std(dim=['time'], keep_attrs=True, skipna=True)
    ed_t2_std.append(ed_std)
    print('ED calculated for ' + ed_mean.attrs['catchment'])

    # then plot up the mean ed values and save to a plot folder
    outputFilePath = os.path.join(filepath, ('Outputs/'))  # temporary file on NCI to store images
    fname = ed_mean.attrs['catchment'] + '_EDmean_t2.pdf'
    dsname = os.path.join(outputFilePath + fname)

    ed_mean.plot()
    plt.title(ed_mean.attrs['catchment'])  # give the plot title the catchment/site name
    plt.axis('equal')  # to make the pixels square
    plt.savefig(fname)
    print('plot saved for ' + ed_mean.attrs['catchment'])
    plt.close()

    xportToAWS(dsname, aws_bucket, fname)
    print('Wrote {} to plot and exported to AWS'.format(fname))

    # then get the mean and std across time at the site level:
    ed_site_name = ed_mean.attrs['catchment']
    ed_t2_site_name.append(ed_site_name)
    ed_site_mean = ed_vals.mean(keep_attrs=True, skipna=True)
    ed_t2_site_mean.append(ed_site_mean.values)
    ed_site_std = ed_vals.std(keep_attrs=True, skipna=True)
    ed_t2_site_std.append(ed_site_std.values)

print('Finished calculating pixel mean Euclidean distance and saving plots')

print('Getting final site level values into a table and saving out')
# use the site level statistics to make a table of ed means, std and difference between times

ed_means_std_table = pd.DataFrame(
    {'Site T1': ed_t1_site_name, 'ED mean T1': ed_t1_site_mean, 'ED std T1': ed_t1_site_std,
     'Site T2': ed_t2_site_name, 'ED mean T2': ed_t2_site_mean, 'ED std T2': ed_t2_site_std})

ed_means_std_table['ED diff.'] = ed_means_std_table['ED mean T2'] - ed_means_std_table['ED mean T1']

# and turn the table into a png table

# get the numbers into 2 decimal places
ed_means_std_table['ED mean T1'] = ed_means_std_table['ED mean T1'].astype(float).round(2)
ed_means_std_table['ED std T1'] = ed_means_std_table['ED std T1'].astype(float).round(2)
ed_means_std_table['ED mean T2'] = ed_means_std_table['ED mean T2'].astype(float).round(2)
ed_means_std_table['ED std T2'] = ed_means_std_table['ED std T2'].astype(float).round(2)
ed_means_std_table['ED diff.'] = ed_means_std_table['ED diff.'].astype(float).round(2)


# make the table
def render_mpl_table(data, col_width=6.0, row_height=0.625, font_size=14,
                     header_color='#40466e', row_colors=['#f1f1f2', 'w'], edge_color='w',
                     bbox=[0, 0, 1, 1], header_columns=0,
                     ax=None, **kwargs):
    if ax is None:
        size = (np.array(data.shape[::-1]) + np.array([0, 1])) * np.array([col_width, row_height])
        fig, ax = plt.subplots(figsize=size)
        ax.axis('off')
    mpl_table = ax.table(cellText=data.values, bbox=bbox, colLabels=data.columns, **kwargs)
    mpl_table.auto_set_font_size(False)
    mpl_table.set_fontsize(font_size)

    for k, cell in mpl_table._cells.items():
        cell.set_edgecolor(edge_color)
        if k[0] == 0 or k[1] < header_columns:
            cell.set_text_props(weight='bold', color='w')
            cell.set_facecolor(header_color)
        else:
            cell.set_facecolor(row_colors[k[0] % len(row_colors)])
    return ax.get_figure(), ax


fig, ax = render_mpl_table(ed_means_std_table, header_columns=0, col_width=4.0)

outputFilePath = os.path.join(filepath, ('Outputs/'))  # temporary file on NCI to store images
fname = 'table_EDmeans.png'
dsname = os.path.join(outputFilePath + fname)
fig.savefig(dsname)

xportToAWS(dsname, aws_bucket, fname)
print('Wrote {} to image table and exported to AWS'.format(fname))

# also save the data as a .csv if want to use in other analysis/table set up
#no need to re-do outputFilePath
fname = 'EDmeansData.csv'
dsname = os.path.join(outputFilePath + fname)
ed_means_std_table.to_csv(dsname)

xportToAWS(dsname, aws_bucket, fname)
print('Wrote {} to csv and exported to AWS'.format(fname))

print('finished script')