import datetime, os, sys
import fiona
import geopandas as gpd

import numpy as np
import rasterio.mask
import rasterio.features
from shapely import geometry
from shapely.geometry import shape
import sys
import xarray as xr

import boto3, warnings, datacube
warnings.filterwarnings('ignore')

from datacube.storage import masking
from datacube.utils import geometry
from datacube.helpers import ga_pq_fuser, write_geotiff
from datacube import Datacube

sys.path.append('/home/550/ns6732/dea-notebooks-prod/10_Scripts')
import DEADataHandling, DEAPlotting, TasseledCapTools

dc = datacube.Datacube(app='MDBA')

def geom_query_output(geom_crs, geom):
    """
    Create datacube query snippet for geometry
    """
    return {
        'x': (geom.bounds[0], geom.bounds[2]),
        'y': (geom.bounds[1], geom.bounds[3]),
        'crs': geom_crs
    	}

def getCatchments(path_to_shapefile):
    catchment_boundaries = os.path.expanduser(path_to_shapefile)
    sub_boundaries = fiona.open(catchment_boundaries)
    return sub_boundaries

def getCatchmentCount(path_to_shapefile):
    catchment_boundaries = os.path.expanduser(path_to_shapefile)
    sub_boundaries = fiona.open(catchment_boundaries)
    return len(sub_boundaries)

def getCatchmentGeomName(i):
    sub_boundaries = getCatchments(path_to_shapefile)
    geom_crs = sub_boundaries.crs_wkt
    geom = shape(sub_boundaries[i]['geometry'])
    geom_query = geom_query_output(geom_crs, geom)
    catchment_name = sub_boundaries[i]['properties']['NAME']
    catchment_name_output = catchment_name.replace(" ", "_")
    return geom_crs, geom, catchment_name_output, geom_query

def last_day_of_month(any_day):
    next_month = any_day.replace(day=28) + datetime.timedelta(days=4)
    return next_month - datetime.timedelta(days=next_month.day)

def developMonthlyQuery(year,month):
	first_day = datetime.date(year,month,1)
	last_day= last_day_of_month(datetime.date(year, month, 1))
	return first_day, last_day

def develop6MonthRollingAverageQuery(year,month):
    first_day = datetime.date(year, month, 1)
    last_day = last_day_of_month(datetime.date(year, month+5, 1))
    return first_day, last_day

def developFiscalQuery(year):
    first_day = datetime.date(year, 7, 1)
    last_day = last_day_of_month(datetime.date(year+1, 6, 1))
    return first_day, last_day

def developSeasonalQuery(year, season):
    if season == 'summer':
        prev_year = year-1
        first_day = developMonthlyQuery(prev_year,12)[0]
        last_day= developMonthlyQuery(year,2)[1]
        return first_day, last_day
    if season == 'autumn':
        first_day = developMonthlyQuery(year,3)[0]
        last_day= developMonthlyQuery(year,5)[1]
        return first_day, last_day
    if season == 'winter':
        first_day = developMonthlyQuery(year,6)[0]
        last_day= developMonthlyQuery(year,8)[1]
        return first_day, last_day
    if season == 'spring':
        first_day = developMonthlyQuery(year,9)[0]
        last_day= developMonthlyQuery(year,11)[1]
        return first_day, last_day

def getMonthName(month):
	month = datetime.date(1900, month, 1).strftime('%b')
	return month

def generateDateString(any_date):
    return datetime.date(int(any_date.split('-')[0]),int(any_date.split('-')[1]),int(any_date.split('-')[2]))

def developSensorDateQuery(any_day,sensor):
    # Allows users to search pre and proceeding dates for imagery within the same pass
    if sensor == 'landsat':
        days_to_search=30
    elif sensor == 'sentinel':
        days_to_search=30
    start_date = generateDateString(any_day) + datetime.timedelta(days=-days_to_search)
    end_date  = generateDateString(any_day) + datetime.timedelta(days=days_to_search)
    return start_date, end_date, sensor


def developSensorTypeQuery(start_date, end_date, sensor):
    if sensor == 'landsat':
        landsat_sensor = []
        if start_date > generateDateString('1984-03-01') and end_date < generateDateString('2013-06-05'):
            landsat_sensor.append('ls5')
        if start_date > generateDateString('1999-04-15') and end_date < datetime.datetime.now().date():
            landsat_sensor.append('ls7')
        if start_date > generateDateString('2013-02-11') and end_date < datetime.datetime.now().date():
            landsat_sensor.append('ls8')
        return landsat_sensor
    if sensor == 'sentinel':
        sentinel_sensor = []
        if start_date > generateDateString('2015-06-23') and end_date < datetime.datetime.now().date():
            sentinel_sensor.append('s2a_ard_granule')
        if start_date > generateDateString('2017-03-07') and end_date < datetime.datetime.now().date():
            sentinel_sensor.append('s2b_ard_granule')
        return sentinel_sensor

def CreateDEADataHandlingload_dataQuery(date,sensor, geom_query, geom_crs):
    sensor_list = developSensorTypeQuery(developSensorDateQuery(date,sensor)[0],developSensorDateQuery(date,sensor)[1],developSensorDateQuery(date,sensor)[2])
    if sensor == 'landsat':
        query = {'time': (developSensorDateQuery(date,sensor)[0],developSensorDateQuery(date,sensor)[1]),
                 }
        query.update(geom_query)
    if sensor == 'sentinel':
        query = {'time': (developSensorDateQuery(date,sensor)[0],developSensorDateQuery(date,sensor)[1]),
                 'output_crs': 'EPSG:3577',
                 'resolution': (-10, 10)
                 }
        query.update(geom_query)
    return sensor_list, query

def loadExportDEAdata(sensor,date_list,bands_of_interest, filter_pq, outputFilePath, path_to_shapefile):
    num_catchments = getCatchmentCount(path_to_shapefile)
    i=0
    while i < num_catchments:
        geom_crs = getCatchmentGeomName(i)[0]
        geom = getCatchmentGeomName(i)[1]
        catchment_name = getCatchmentGeomName(i)[2]
        geom_query = getCatchmentGeomName(i)[3]


        print("Now processing..... " + catchment_name)
        if sensor == 'landsat':
            for date in date_list:
                print("Start and end ranges for imagery searching are: " + str(developSensorDateQuery(date,sensor)[0]) + ", " + str(developSensorDateQuery(date,sensor)[1]))
                sensor_list = CreateDEADataHandlingload_dataQuery(date,sensor,geom_query, geom_crs)[0]
                
                for sensorID in sensor_list:

                    print("Now processing sensor: " + sensorID)
                    query = CreateDEADataHandlingload_dataQuery(date,sensor,geom_query, geom_crs)[1]
                    ds, crs, affine = DEADataHandling.load_nbarx(dc=dc, query=query, sensor=sensorID, product='nbart', bands_of_interest=bands_of_interest, filter_pq=filter_pq)
                    print(ds)
                    if ds is None:
                        print('No data loaded from DEA - lack of landsat satellite data in AOI for selected date: {}'.format(date))
                        
                    else:
                        if len(ds.time) >= 1:
                            data = ds
                            shp = gpd.read_file(path_to_shapefile)
                            shp = shp.to_crs({'init': 'epsg:3577'})                        
                            mask = rasterio.features.geometry_mask(((feature['geometry']) for feature in shp.iterfeatures()),
                                           out_shape=data.geobox.shape,
                                           transform=data.geobox.affine,
                                           all_touched=False,
                                           invert=False
                                           )
                            mask_xr = xr.DataArray(mask, dims = ('y','x'))
                            ds = data.where(mask_xr==False)

                            for timestep in range(len(ds.time)):
                                timestep_date = np.datetime_as_string(ds.time.isel(time=timestep))[0:10]
                                dsname=catchment_name+'_'+sensorID+'_'+timestep_date+'.tif'
                                write_geotiff(outputFilePath+dsname, ds.isel(time = timestep))
                                exportToAWS(outputFilePath+dsname, 'data-cube-outputs', dsname)
                                print('Wrote {} to GeoTiff and exported to AWS'.format(dsname))
                    

        if sensor == 'sentinel':
            for date in date_list:
                print("Start and end ranges for imagery searching are: " + str(developSensorDateQuery(date,sensor)[0]) + ", " + str(developSensorDateQuery(date,sensor)[1]))
                sensor_list = CreateDEADataHandlingload_dataQuery(date,sensor,geom_query, geom_crs)[0]
                print(sensor_list)
                for sensorID in sensor_list:
                    query = CreateDEADataHandlingload_dataQuery(date,sensor,geom_query, geom_crs)[1]
                    sensor_code = sensorID[:3] 

                    ds = DEADataHandling.load_clearsentinel2(dc=dc, query=query, sensors=[str(sensor_code)], bands_of_interest = ['fmask', 'nbart_blue', 'nbart_green', 'nbart_red', 'nbart_nir_1','nbart_nir_2','nbart_swir_2','nbart_swir_3'], masked_prop=0.0, satellite_metadata=False)

                    if ds is None:
                            print('No data loaded from DEA - lack of sentinel satellite data in AOI for selected date: {}'.format(date))
                            continue
                    else:
                            if len(ds.time) >= 1:

                                data = ds
                                shp = gpd.read_file(path_to_shapefile)
                                shp = shp.to_crs({'init': 'epsg:3577'})
                                mask = rasterio.features.geometry_mask(((feature['geometry']) for feature in shp.iterfeatures()),
                                           out_shape=data.geobox.shape,
                                           transform=data.geobox.affine,
                                           all_touched=False,
                                           invert=False
                                           )
                                mask_xr = xr.DataArray(mask, dims = ('y','x'))
                                ds = data.where(mask_xr==False)

                                for timestep in range(len(ds.time)):
                                    timestep_date = np.datetime_as_string(ds.time.isel(time=timestep))[0:10]
                                    dsname=catchment_name+'_'+sensorID+'_'+timestep_date+'.tif'
                                    write_geotiff(outputFilePath+dsname, ds.isel(time = timestep))
                                    exportToAWS(outputFilePath+dsname, 'data-cube-outputs', dsname)
                                    print('Wrote {} to GeoTiff and exported to AWS'.format(dsname))
        i += 1


def exportToAWS(fileName, bucketName, outputName):
    s3_client = boto3.client('s3')
    s3_client.upload_file(fileName,bucketName,outputName)
    os.remove(fileName)

if __name__ == "__main__":
	print("Loading DEA and exporting images....")
	path_to_shapefile = '/home/550/ns6732/dea-notebooks-prod/MDBA/Boundaries/Yellow_Clay.shp'
	outputFilePath = '/home/550/ns6732/dea-notebooks-prod/MDBA/outputs/'
	sensorName = 'landsat'
	date_list = ['2016-09-01'] # yyy-mm-dd

	bands_of_interest = []#add bands here

	filter_pq = 'True'
	loadExportDEAdata(sensorName,date_list,bands_of_interest,filter_pq, outputFilePath, path_to_shapefile)






